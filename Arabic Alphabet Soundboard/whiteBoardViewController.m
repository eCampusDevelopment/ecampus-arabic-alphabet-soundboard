//
//  whiteBoardViewControllerViewController.m
//  Arabic Alphabet Soundboard
//
//  Created by Nick Alto on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "whiteBoardViewController.h"
#import <MessageUI/MFMailComposeViewController.h>

@implementation whiteBoardViewController
@synthesize whiteBoardType = _whiteBoardType;
@synthesize whiteboard = _whiteBoard;

/*********************************************************************
 * Purpose: Initialize whiteboard's buttons/view/etc
 *
 * Notes: Most of the code is programatically creating each button and
 * It's corresponding CALayer which allows for a flat, square button.
 *
 *********************************************************************/

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andContext:(CGContextRef)ctx andFlag:(int)flag
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {
        whiteBoardType = flag;
        cacheContext = ctx;
        UIImage *background = [UIImage imageNamed:@"ipadBackground.png"];
        indicator = [UIButton buttonWithType:UIButtonTypeCustom];
        
        scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(-2, 127, 904, 623)];

        self.view.backgroundColor = [UIColor colorWithPatternImage:background];
        if(whiteBoardType)
            whiteboard = [[whiteBoardView alloc]initWithFrame:CGRectMake(0, 0, 2000, 1200) andContext:cacheContext];
        else
            whiteboard = [[whiteBoardView alloc]initWithFrame:CGRectMake(-2, 127, 904, 623) andContext:cacheContext];

        whiteboard.backgroundColor = [UIColor whiteColor];
        whiteboard.opaque = YES;
        whiteboard.contentMode = UIViewContentModeCenter;
        //Whiteboard
        
        CALayer *whiteBoardBezel = whiteboard.layer;
        whiteBoardBezel.backgroundColor = [[UIColor whiteColor] CGColor];
        whiteBoardBezel.borderColor = [[UIColor blackColor] CGColor];
        whiteBoardBezel.cornerRadius = 0;
        whiteBoardBezel.borderWidth = 3.2f;

        if(whiteBoardType)
        {
        
        CALayer *scrollViewLayer = scrollView.layer;
        scrollViewLayer.backgroundColor = [[UIColor whiteColor] CGColor];
        scrollViewLayer.borderColor = [[UIColor blackColor] CGColor];
        scrollViewLayer.cornerRadius = 0;
        scrollViewLayer.borderWidth = 3.2f;
        
        
        scrollView.minimumZoomScale = .4;
        scrollView.backgroundColor = [UIColor grayColor];
        scrollView.contentSize = whiteboard.frame.size;
        [scrollView setDelegate:self];
        
        scrollView.contentMode = (UIViewContentModeCenter);
        scrollView.maximumZoomScale = 2.0;
        for (UIGestureRecognizer *gestureRecognizer in scrollView.gestureRecognizers) {     
            if ([gestureRecognizer  isKindOfClass:[UIPanGestureRecognizer class]])
            {
                UIPanGestureRecognizer *panGR = (UIPanGestureRecognizer *) gestureRecognizer;
                panGR.minimumNumberOfTouches = 2;               
            }
        }

        [scrollView addSubview:whiteboard];
        [self.view addSubview:scrollView];
        }
        else {
            [self.view addSubview:whiteboard];
        }

        
        //Whiteboard title 
        UIButton *buttonTitle = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonTitle.frame = CGRectMake(25, 16, 800, 79);
        buttonTitle.backgroundColor = [UIColor clearColor];
        buttonTitle.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:65.0];
        buttonTitle.titleLabel.textAlignment = UITextAlignmentLeft;
        [buttonTitle setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [buttonTitle setTitleColor:[UIColor colorWithRed:.745 green:.7019 blue:.917 alpha:1] forState:UIControlStateNormal];
        [buttonTitle setTitle:@"Whiteboard" forState:UIControlStateNormal];
        [buttonTitle setUserInteractionEnabled:NO];
        [self.view addSubview:buttonTitle];
        

        
        //Whiteboard Side Bar
        UIView *wbSideBar  = [[UIView alloc]initWithFrame:CGRectMake(900, 127, 126, 623)];
        wbSideBar.backgroundColor = [UIColor whiteColor];
        CALayer *wbSideBarLayer = wbSideBar.layer;
        wbSideBarLayer.backgroundColor = [[UIColor whiteColor] CGColor];
        wbSideBarLayer.borderColor = [[UIColor blackColor] CGColor];
        wbSideBarLayer.cornerRadius = 0;
        wbSideBarLayer.borderWidth = 3.2f;
        [self.view addSubview:wbSideBar];
        
        
        //Draw button
        drawMode = [UIButton buttonWithType:UIButtonTypeCustom];
        drawMode.frame = CGRectMake(900, 127, 126, 155);
        [drawMode setTitle:@"Draw" forState:UIControlStateNormal];
        [drawMode setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        drawMode.tag = 1;
        CALayer *drawLayer = drawMode.layer;
        drawLayer.borderColor = [[UIColor blackColor]CGColor];
        drawLayer.cornerRadius = 0;
        drawLayer.borderWidth = 3.2f;
        drawLayer.backgroundColor = [[UIColor clearColor]CGColor];
        [drawMode addTarget:self action:@selector(drawMode:) forControlEvents:UIControlEventTouchUpInside];
        drawMode.backgroundColor = [UIColor colorWithRed:.4549 green:.3843 blue:.6313 alpha:.2];
        [self.view addSubview:drawMode];
        
        
        //Erase button
        eraseMode = [UIButton buttonWithType:UIButtonTypeCustom];
        eraseMode.tag = 0;
        [eraseMode setTitle:@"Erase" forState:UIControlStateNormal];
        [eraseMode setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        eraseMode.frame = CGRectMake(900, 279, 126, 155);
        [eraseMode addTarget:self action:@selector(drawMode:) forControlEvents:UIControlEventTouchUpInside];
        CALayer *eraseModeLayer = eraseMode.layer;
        eraseModeLayer.borderColor = [[UIColor blackColor]CGColor];
        eraseModeLayer.backgroundColor = [[UIColor clearColor]CGColor];
        eraseModeLayer.cornerRadius = 0;
        eraseModeLayer.borderWidth = 3.2f;
        eraseMode.backgroundColor = [UIColor colorWithRed:.4549 green:.3843 blue:.6313 alpha:.4];
        [self.view addSubview:eraseMode];
        
        
        //Clear button
        clear = [UIButton buttonWithType:UIButtonTypeCustom];
        clear.frame = CGRectMake(900, 432, 126, 155);
        [clear setTitle:@"Clear" forState:UIControlStateNormal];
        [clear setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        clear.tag = -1;
        CALayer *clearLayer = clear.layer;
        [clear addTarget:self action:@selector(drawMode:) forControlEvents:UIControlEventTouchUpInside];
        clearLayer.borderColor = [[UIColor blackColor]CGColor];
        clearLayer.cornerRadius = 0;
        clearLayer.backgroundColor = [[UIColor clearColor]CGColor];
        clearLayer.borderWidth = 3.2f;
        clear.backgroundColor = [UIColor colorWithRed:.4549 green:.3843 blue:.6313 alpha:.6];
        [self.view addSubview:clear];
        
        
        //email Button
        email = [UIButton buttonWithType:UIButtonTypeCustom];
        email.frame = CGRectMake(900, 585, 126, 165);
        [email setTitle:@"Save" forState:UIControlStateNormal];
        [email setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        email.tag = 2;
        CALayer *emailLayer = email.layer;
        emailLayer.borderColor = [[UIColor blackColor]CGColor];
        emailLayer.cornerRadius = 0;
        emailLayer.borderWidth = 3.2f;
        emailLayer.backgroundColor = [[UIColor clearColor]CGColor];
        [email addTarget:self action:@selector(drawMode:) forControlEvents:UIControlEventTouchUpInside];
        email.backgroundColor = [UIColor colorWithRed:.4549 green:.3843 blue:.6313 alpha:.8];
        [self.view addSubview:email];
        
        
        //Whiteboard Button to return to Soundboard
        UIButton *whiteBoardButton = [UIButton buttonWithType:UIButtonTypeCustom];
        whiteBoardButton.frame = CGRectMake(902, 10, 112, 80);
        whiteBoardButton.backgroundColor = [UIColor clearColor];
        whiteBoardButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
        whiteBoardButton.titleLabel.textAlignment = UITextAlignmentLeft;
        CALayer *whiteBoardLayer = whiteBoardButton.layer;
        whiteBoardLayer.backgroundColor = [[UIColor clearColor] CGColor];
        whiteBoardLayer.borderColor = [[UIColor colorWithRed:.2313 green:.1333 blue:.4274 alpha:1] CGColor];
        whiteBoardLayer.cornerRadius = 5.0f;
        whiteBoardLayer.borderWidth = 1.2f;
        [whiteBoardButton setTitleColor:[UIColor colorWithRed:.2313 green:.1333 blue:.4274 alpha:1] forState:UIControlStateNormal];
        [whiteBoardButton setTitle:@"Soundboard" forState:UIControlStateNormal];
        [whiteBoardButton addTarget:self action:@selector(popOffWhiteBoard:) forControlEvents:UIControlEventTouchDown];
        [whiteBoardButton setUserInteractionEnabled:YES];
        [self.view addSubview:whiteBoardButton];
        
        //Indicator CALayer 
        CALayer *indicatorLayer = indicator.layer;
        indicatorLayer.borderColor = [[UIColor blackColor]CGColor];
        indicatorLayer.cornerRadius = 0;
        indicatorLayer.borderWidth = 3.2f;
        indicatorLayer.backgroundColor = [[UIColor clearColor]CGColor];
        indicator.backgroundColor = [UIColor colorWithRed:.529 green:.8196 blue:1 alpha:.4];
        indicator.frame = CGRectMake(drawMode.frame.origin.x - 10, drawMode.frame.origin.y, 13, drawMode.frame.size.height);
        [self.view addSubview:indicator];
        
    }
    return self;
}


-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView1 {
    
    scrollView1.contentMode = (UIViewContentModeScaleAspectFit);
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)? 
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)? 
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    
    whiteboard.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX, 
                                 scrollView.contentSize.height * 0.5 + offsetY);
    return whiteboard;
}

/*********************************************************************
 * Purpose: Called for each button on whiteboard's sidebar
 *
 * Notes:   0 = Erase Mode
 *          1 = Erase Mode
 *         -1 = Clear Canvas - currently needs to be double tapped (bug)
 *          2 = Email  
 *
 *********************************************************************/

-(IBAction)drawMode:(id)sender {
    
    if(WBVCPrintFlag)
        NSLog(@"drawMode called\n");
    
    UIButton *temp = sender;
    
    CALayer *indicatorLayer = indicator.layer;
    indicatorLayer.borderColor = [[UIColor blackColor]CGColor];
    indicatorLayer.cornerRadius = 0;
    indicatorLayer.borderWidth = 3.2f;
    indicatorLayer.backgroundColor = [[UIColor clearColor]CGColor];
    indicator.backgroundColor = [UIColor colorWithRed:.529 green:.8196 blue:1 alpha:.4];
    
    
    if(temp.tag == 1) {
        
        [indicator removeFromSuperview];
        if(WBVCPrintFlag)
            NSLog(@"draw\n");
        
        [whiteboard draw];
        indicator.frame = CGRectMake(drawMode.frame.origin.x - 10, drawMode.frame.origin.y, 13, drawMode.frame.size.height);
        [self.view addSubview:indicator];
        
        
    }
    else if (temp.tag == 0) {
        [indicator removeFromSuperview];
        
        if(WBVCPrintFlag)
            NSLog(@"erase\n");
        
        indicator.frame = CGRectMake(eraseMode.frame.origin.x - 10, eraseMode.frame.origin.y, 13, eraseMode.frame.size.height);
        
        [self.view addSubview:indicator];
        
        [whiteboard erase];
    }
    else if (temp.tag == -1)    {
        
        [whiteboard clear];
    }
    else if(temp.tag = 2) {
        
        // Mail code commented out, because the mail functionality is still debated
        // cannot implement until a decision has been made on the mail functionality. 
        /*MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        
        [picker setSubject:@"Whiteboard Data"];
        
        
        // Set up recipients
        NSArray *toRecipients = [NSArray arrayWithObject:@"test@nickalto.com"];  
        
        [picker setToRecipients:toRecipients];
        
        // Attach an image to the email
        //NSString *path = [[NSBundle mainBundle] pathForResource:@"rainy" ofType:@"png"];
        //NSData *myData = [NSData dataWithContentsOfFile:path];
        //[picker addAttachmentData:myData mimeType:@"image/png" fileName:@"rainy"];
        
        // Fill out the email body text
        NSString *emailBody = @"Arabic 111 Whiteboard";
        [picker setMessageBody:emailBody isHTML:NO];
        
        [self presentModalViewController:picker animated:YES];
         */
        
        //[whiteboard saveImage];
        [whiteboard clear];
        
    }
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload {
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
	if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight)
        return  TRUE;
    else 
        return FALSE;
}

-(IBAction)popOffWhiteBoard:(id)sender {
    
    cacheContext = [whiteboard returnCache];
    [self dismissModalViewControllerAnimated:TRUE];
    
}

@end
