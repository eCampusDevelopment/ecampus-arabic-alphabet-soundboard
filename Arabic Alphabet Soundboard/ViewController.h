//
//  ViewController.h
//  Arabic Alphabet Soundboard
//
//  Created by Nick Alto on 3/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import "CreditsViewController.h"
#import <QuartzCore/QuartzCore.h> 
#import "whiteBoardViewController.h"


#define ARABIC 0
#define HEBREW 1

// 0 - Arabic
// 1 - Hebrew
// 2 - ???

#define VCPrintFlag 0


@interface ViewController : UIViewController {
    
    IBOutlet UIButton *info;                    // Info button top right hand corner
    CreditsViewController *creditsController;   // VC corresponding to info button
    NSMutableArray *buttonArr ;                 // Array holding each button's data
    NSMutableArray *labelArr;                   // Array holding each label's data
    CGContextRef cacheContext;                  // context of the whiteboard
    whiteBoardViewController *wbvcStandard;     // VC corresponding to the whiteboard
    whiteBoardViewController *wbvcLarger;       // VC corresponding to the whiteboard
    
}

@property (nonatomic, strong) whiteBoardViewController *wbvcStandard;
// Public Methods
- (IBAction)pressed:(id)sender;
- (IBAction)labelPressed:(id)sender;
- (IBAction)infoPressed:(id)sender;
- (void)showMessage;
- (void)showCredits;


@end
