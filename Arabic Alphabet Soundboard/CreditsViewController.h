//
//  CreditsViewController.h
//  Arabic Alphabet Soundboard
//
//  Created by Nick Alto on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreditsViewController : UIViewController
{
    IBOutlet UIButton *button;
}

@property (nonatomic) IBOutlet UIButton *button;

-(IBAction)dismissCredits:(id)sender;
@end
