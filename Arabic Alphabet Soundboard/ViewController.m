//
//  ViewController.m
//  Arabic Alphabet Soundboard
//
//  Created by Nick Alto on 3/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

int languageFlag = ARABIC;
BOOL flipFlag = false;

// 0 - Arabic
// 1 - Hebrew
// 2 - ???

@implementation ViewController
@synthesize wbvcStandard = _wbvcStandard;
/*********************************************************************
 * Purpose: Initialize view controllers, as well as the soundboard
 * characters/buttons/alignment, etc. 
 *
 * Notes: Creates arrays for each language, including each button's 
 * own CGRect's, same for phonetic desciptions and for characters, 
 * loading determined by the languageFlag value. Initialize all buttons
 * corresponding CALayers if necessary and add them to the view.
 *
 *********************************************************************/

 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        buttonArr   = [[NSMutableArray alloc]init];
        labelArr    = [[NSMutableArray alloc]init];
        
        NSArray *arabicButtonPos    = nil;
        NSArray *arabicLabelPos     = nil;
        NSArray *arabicLetters      = nil;
        NSArray *arabicLabels       = nil;
        cacheContext                = nil;
        wbvcLarger = [[whiteBoardViewController alloc]initWithNibName:@"whiteBoardViewController" bundle:nil andContext:cacheContext andFlag:1];
        wbvcStandard = [[whiteBoardViewController alloc]initWithNibName:@"whiteBoardViewController" bundle:nil andContext:cacheContext andFlag:0];
        creditsController = [[CreditsViewController alloc]initWithNibName:@"CreditsViewController" bundle:nil];
        
        //Only allocate for selected language
        if(languageFlag == ARABIC) {
            
            arabicButtonPos = [[NSArray alloc]initWithObjects:
                               //First row y value 126 - centered, y value 132 - alignment top
                               [NSValue valueWithCGRect:CGRectMake(29,  126, 105, 105)],
                               [NSValue valueWithCGRect:CGRectMake(177, 126, 105, 105)],
                               [NSValue valueWithCGRect:CGRectMake(319, 126, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(460, 132, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(608, 132, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(754, 132, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(899, 132, 105, 105)],
                               
                               
                               //Second row y value 278 - centered, y value 283 - alignment top
                               [NSValue valueWithCGRect:CGRectMake(29,  283, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(177, 283, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(319, 283, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(460, 278, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(608, 278, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(754, 283, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(899, 283, 105, 105)], 
                               
                               //Third row y value 434 - centered, y value 439/444 - alignment top
                               [NSValue valueWithCGRect:CGRectMake(29,  444, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(177, 439, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(319, 434, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(460, 434, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(608, 439, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(754, 439, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(899, 439, 105, 105)], 
                               
                               //Forth row y value 588/583 - centered, y value 598 - alignment top
                               [NSValue valueWithCGRect:CGRectMake(29,  588, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(177, 598, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(319, 598, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(460, 583, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(608, 583, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(754, 588, 105, 105)], 
                               [NSValue valueWithCGRect:CGRectMake(899, 598, 105, 105)], 
                               
                               
                               nil];
            
            arabicLabelPos = [[NSArray alloc]initWithObjects:
                              
                              //First Row
                              [NSValue valueWithCGRect:CGRectMake(29,  233, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(177, 233, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(319, 233, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(460, 233, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(608, 233, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(754, 233, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(899, 233, 105, 37)],
                              
                              //Second Row
                              [NSValue valueWithCGRect:CGRectMake(29,  384, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(177, 384, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(319, 384, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(460, 384, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(608, 384, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(754, 384, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(899, 384, 105, 37)],
                              
                              //Third Row
                              [NSValue valueWithCGRect:CGRectMake(29,  543, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(177, 543, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(319, 543, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(460, 543, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(608, 543, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(754, 543, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(899, 543, 105, 37)],
                              
                              //Forth Row
                              [NSValue valueWithCGRect:CGRectMake(29,  696, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(177, 696, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(319, 696, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(460, 696, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(608, 696, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(754, 696, 105, 37)],
                              [NSValue valueWithCGRect:CGRectMake(899, 696, 105, 37)],
                              
                              nil ];
            
            arabicLetters = [[NSArray alloc]initWithObjects:
                             
                             /*row 1 [0] [1] [2] [3] [4] [5] [6] */
                             /*[0]*/ [NSString stringWithFormat:@"خ"], /*[1]*/  [NSString stringWithFormat:@"ح"],
                             /*[2]*/ [NSString stringWithFormat:@"ج"], /*[3]*/  [NSString stringWithFormat:@"ث"],
                             /*[4]*/ [NSString stringWithFormat:@"ت"], /*[5]*/  [NSString stringWithFormat:@"ب"], 
                             /*[6]*/ [NSString stringWithFormat:@"ا"], 
                             
                             /*row 2 [7] [8] [9] [10] [11] [12] [13] */
                             /*[7]*/ [NSString stringWithFormat:@"ص"], /*[8]*/  [NSString stringWithFormat:@"ش"],
                             /*[9]*/ [NSString stringWithFormat:@"س"], /*[10]*/ [NSString stringWithFormat:@"ز"], 
                             /*[11]*/ [NSString stringWithFormat:@"ر"], /*[12]*/ [NSString stringWithFormat:@"ذ"], 
                             /*[13]*/ [NSString stringWithFormat:@"د"], 
                             
                             /*row 3 [14] [15] [16] [17] [18] [19] [20] */
                             /*[14]*/ [NSString stringWithFormat:@"ق"], /*[15]*/ [NSString stringWithFormat:@"ف"], 
                             /*[16]*/ [NSString stringWithFormat:@"غ"], /*[17]*/ [NSString stringWithFormat:@"ع"], 
                             /*[18]*/ [NSString stringWithFormat:@"ظ"], /*[19]*/ [NSString stringWithFormat:@"ط"], 
                             /*[20]*/ [NSString stringWithFormat:@"ض"], 
                             
                             /*row 4 [21] [22] [23] [24] [25] [26] [27] */
                             /*[21]*/ [NSString stringWithFormat:@"ي"], /*[22]*/ [NSString stringWithFormat:@"و"], 
                             /*[23]*/ [NSString stringWithFormat:@"هـ"], /*[24]*/ [NSString stringWithFormat:@"ن"], 
                             /*[25]*/ [NSString stringWithFormat:@"م"], /*[25]*/ [NSString stringWithFormat:@"ل"], 
                             /*[26]*/ [NSString stringWithFormat:@"ك"], nil];
            
            
            arabicLabels = [[NSArray alloc]initWithObjects:
                            
                            /*row 1 [0] [1] [2] [3] [4] [5] [6] */
                            /*[0]*/ [NSString stringWithFormat:@"khaa"], /*[1]*/   [NSString stringWithFormat:@"Haa"],
                            /*[2]*/ [NSString stringWithFormat:@"jiim"], /*[3]*/   [NSString stringWithFormat:@"thaa"],
                            /*[4]*/ [NSString stringWithFormat:@"taa"], /*[5]*/    [NSString stringWithFormat:@"baa"], 
                            /*[6]*/ [NSString stringWithFormat:@"alif"], 
                            
                            /*row 2 [7] [8] [9] [10] [11] [12] [13] */
                            /*[7]*/ [NSString stringWithFormat:@"Saad"], /*[8]*/   [NSString stringWithFormat:@"sheen"],
                            /*[9]*/ [NSString stringWithFormat:@"seen"], /*[10]*/  [NSString stringWithFormat:@"zaay"], 
                            /*[11]*/ [NSString stringWithFormat:@"raa"], /*[12]*/  [NSString stringWithFormat:@"dhaal"], 
                            /*[13]*/ [NSString stringWithFormat:@"daal"], 
                            
                            /*row 3 [14] [15] [16] [17] [18] [19] [20] */
                            /*[14]*/ [NSString stringWithFormat:@"qaaf"], /*[15]*/ [NSString stringWithFormat:@"faa"], 
                            /*[16]*/ [NSString stringWithFormat:@"ghayn"], /*[17]*/[NSString stringWithFormat:@"'ayn"], 
                            /*[18]*/ [NSString stringWithFormat:@"DHaa"], /*[19]*/ [NSString stringWithFormat:@"Taa"], 
                            /*[20]*/ [NSString stringWithFormat:@"Daad"], 
                            
                            /*row 4 [21] [22] [23] [24] [25] [26] [27] */
                            /*[21]*/ [NSString stringWithFormat:@"yaa"], /*[22]*/  [NSString stringWithFormat:@"waaw"], 
                            /*[23]*/ [NSString stringWithFormat:@"haa"], /*[24]*/  [NSString stringWithFormat:@"nuun"], 
                            /*[25]*/ [NSString stringWithFormat:@"miim"], /*[25]*/ [NSString stringWithFormat:@"laam"], 
                            /*[26]*/ [NSString stringWithFormat:@"kaaf"], nil];
            
        }
        
        
        UIImage *background = [UIImage imageNamed:@"ipadBackground.png"];
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:background];
        
        //Whiteboard Title
        UIButton *buttonTitle = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonTitle.frame = CGRectMake(0, 16, 800, 79);
        buttonTitle.backgroundColor = [UIColor clearColor];
        buttonTitle.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:65.0];
        buttonTitle.titleLabel.textAlignment = UITextAlignmentLeft;
        [buttonTitle setTitleColor:[UIColor colorWithRed:.745 green:.7019 blue:.917 alpha:1] forState:UIControlStateNormal];
        if(languageFlag == ARABIC)
            [buttonTitle setTitle:@"Arabic Alphabet Soundboard" forState:UIControlStateNormal];
        [buttonTitle setUserInteractionEnabled:NO];
        [self.view addSubview:buttonTitle];
        
        
        
        //ARABIC 111 Button
        UIButton *classButton = [UIButton buttonWithType:UIButtonTypeCustom];
        classButton.frame = CGRectMake(855, 15, 150, 40);
        classButton.backgroundColor = [UIColor clearColor];
        classButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:25.0];
        classButton.titleLabel.textAlignment = UITextAlignmentLeft;
        
        CALayer *classLayer = classButton.layer;
        classLayer.backgroundColor = [[UIColor clearColor] CGColor];
        classLayer.borderColor = [[UIColor colorWithRed:.2313 green:.1333 blue:.4274 alpha:1] CGColor];
        classLayer.cornerRadius = 5.0f;
        classLayer.borderWidth = 1.2f;
        [classButton setTitleColor:[UIColor colorWithRed:.2313 green:.1333 blue:.4274 alpha:1] forState:UIControlStateNormal];
        [classButton setTitle:@"ARABIC 111" forState:UIControlStateNormal];
        [buttonTitle setUserInteractionEnabled:NO];
        [self.view addSubview:classButton];
        
        UIButton *whiteBoardButton = [UIButton buttonWithType:UIButtonTypeCustom];
        whiteBoardButton.frame = CGRectMake(855, 70, 115, 25);
        whiteBoardButton.backgroundColor = [UIColor clearColor];
        whiteBoardButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
        whiteBoardButton.titleLabel.textAlignment = UITextAlignmentLeft;
        
        CALayer *whiteBoardLayer = whiteBoardButton.layer;
        whiteBoardLayer.backgroundColor = [[UIColor clearColor] CGColor];
        whiteBoardLayer.borderColor = [[UIColor colorWithRed:.2313 green:.1333 blue:.4274 alpha:1] CGColor];
        whiteBoardLayer.cornerRadius = 5.0f;
        whiteBoardLayer.borderWidth = 1.2f;
        [whiteBoardButton setTitleColor:[UIColor colorWithRed:.2313 green:.1333 blue:.4274 alpha:1] forState:UIControlStateNormal];
        [whiteBoardButton setTitle:@"Whiteboard" forState:UIControlStateNormal];
        [whiteBoardButton addTarget:self action:@selector(flipView:) forControlEvents:UIControlEventTouchDown];
        [buttonTitle setUserInteractionEnabled:YES];
        [self.view addSubview:whiteBoardButton];
        
        
        
        // Array of arabic characters from 0 (top left to 27 - bottom right*/
        
        for (int i = 0; i <= 27; i++){
            //Allocate memory for buttons assigning frame, tag, etc.
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.tag = i;
            
            if(languageFlag == ARABIC) { //arabic
                //Set necessary offsets to properly align characters
                [button setFrame:[[arabicButtonPos objectAtIndex:i]CGRectValue]];
                [button setTitle:[arabicLetters objectAtIndex:i] forState:UIControlStateNormal];
                
                if((button.frame.origin.y == 132) || (button.frame.origin.y == 283) || 
                   (button.frame.origin.y == 444) || (button.frame.origin.y == 439) ||
                   (button.frame.origin.y == 598)) {
                    
                    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
                    button.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
                }
                else {
                    
                    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
                    button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                }
            }
            
            button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:51.0];
            [button setTitleColor:[UIColor colorWithRed:.33 green:.255 blue:.545 alpha:1] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor colorWithRed:.67 green:.44 blue:.8196 alpha:1] forState:UIControlStateHighlighted];
            [button addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchDown];
            [button addTarget:self action:@selector(pressedRelease:) forControlEvents:UIControlEventTouchUpInside];
            [button addTarget:self action:@selector(pressedRelease:) forControlEvents:UIControlEventTouchCancel];
            [button addTarget:self action:@selector(pressedRelease:) forControlEvents:UIControlEventTouchDragExit];
            [self.view addSubview:button];
            
            [buttonArr addObject:button];
            
            //Allocate another button for label and initialize it
            UIButton *labelButton = [UIButton buttonWithType:UIButtonTypeCustom];
            
            if(languageFlag == ARABIC) {
                
                labelButton.frame = [[arabicLabelPos objectAtIndex:i]CGRectValue];
                [labelButton setTitle:[arabicLabels objectAtIndex:i] forState:UIControlStateNormal];
            }
            
            labelButton.tag = i;
            labelButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:25.0];
            [labelButton setTitleColor:[UIColor colorWithRed:.63 green:.5568 blue:.8705 alpha:1] forState:UIControlStateNormal];
            [labelButton setTitleColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1] forState:UIControlStateHighlighted];
            labelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            labelButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            [labelButton addTarget:self action:@selector(labelPressed:) forControlEvents:UIControlEventTouchDown];
            
            [labelArr addObject:labelButton];
            [self.view addSubview:labelButton];
            
        }
        
    }
    return self;
}

/*********************************************************************
 * Purpose: Change view to the whiteboard by flipping the soundboard
 *
 * Notes: Whiteboard view controller is allocated in the init function
 * which allows for saving of context on the whiteboard.
 *
 *********************************************************************/

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(flipFlag)
    {
        if(buttonIndex == 1)
        {
            NSLog(@"Standard selected\n");

            wbvcStandard.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            wbvcStandard.modalPresentationStyle = UIModalPresentationFullScreen;
            [self.navigationController presentModalViewController:wbvcStandard animated:TRUE];
            
        }
        else if (buttonIndex == 0)
        {
            NSLog(@"Scalable selected\n");

            wbvcLarger.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            wbvcLarger.modalPresentationStyle = UIModalPresentationFullScreen;
            [self.navigationController presentModalViewController:wbvcLarger animated:TRUE];
        }
        flipFlag = false;
    }
    
    
}
-(IBAction)flipView:(id)sender {
    
    if(VCPrintFlag)
        NSLog(@"flipView \n");
// Commented out because the larger scallable board is not as stable as the fixed sized whiteboard
//    flipFlag = TRUE;
//    UIAlertView *setup = [[UIAlertView alloc] initWithTitle:@"Select Whiteboard Type"                                                                                                                           
//                                                      message:@"Would you like a larger scalable whiteboard or a standard whiteboard?" 
//                                                     delegate:self
//                                            cancelButtonTitle:@"Larger" 
//                                          otherButtonTitles:@"Standard", nil];
//    [setup show];
    wbvcStandard.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    wbvcStandard.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.navigationController presentModalViewController:wbvcStandard animated:TRUE];


}


- (void)viewDidLoad {
    
    [self showMessage];
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
}

/*********************************************************************
 * Purpose: Display message when application is first launched. 
 *
 * Notes: N/A
 *
 *********************************************************************/

- (void)showMessage {
    
    UIAlertView *welcome = [[UIAlertView alloc] initWithTitle:@"Welcome"                                                                                                                           
                                                      message:@"Click each letter and the name below to hear the pronounciation" 
                                                     delegate:self
                                            cancelButtonTitle:@"Dismiss" 
                                            otherButtonTitles:nil];
    [welcome show];
}

/*********************************************************************
 * Purpose: Show credits modal view controller.
 *
 * Notes: See CreditsViewController.h/m
 *
 *********************************************************************/

- (void)showCredits {
    
    creditsController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentModalViewController:creditsController animated:YES];
}

- (void)viewDidUnload {
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

/*********************************************************************
 * Purpose: Determines acceptable orientations
 *
 * Notes: N/A
 *
 *********************************************************************/

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    if(interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
        return TRUE;
    else 
        return FALSE;
    
}

/*********************************************************************
 * Purpose: Displays a "glow" effect behind each character when touched
 *
 * Notes: Resets glow when released. 
 *
 *********************************************************************/

-(IBAction)pressedRelease:(id)sender {
    
    NSLog(@"pressedrelease\n");
    UIButton *temp = (UIButton *)sender;
    temp.backgroundColor = [UIColor clearColor];
    
}

/*********************************************************************
 * Purpose: Corresponds to each character, plays associated sound byte
 *
 * Notes: N/A
 *
 *********************************************************************/

- (IBAction)pressed:(id)sender {
    
    UIButton *temp = (UIButton *)sender;
    NSString *toneFilename;
    int letterId = temp.tag;
    
    if(VCPrintFlag)
        NSLog(@"Label: %d\n",temp.tag); 
    
    temp.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"glow.png"]];
    
    switch (letterId) {
        case 0:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter00"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 1:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter01"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 2:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter02"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 3:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter03"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 4:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter04"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 5:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter05"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 6:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter06"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 7:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter07"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 8:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter08"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 9:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter09"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 10:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter10"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 11:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter11"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 12:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter12"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 13:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter13"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 14:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter14"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 15:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter15"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 16:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter16"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 17:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter17"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 18:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter18"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 19:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter19"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 20:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter20"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 21:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter21"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 22:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter22"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 23:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter23"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 24:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter24"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 25:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter25"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 26:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter26"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        case 27:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"letter27"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            break;
        }
        default:
        {
            break;
        }
            
            
    }
    
}

/*********************************************************************
 * Purpose: Corresponds to each label, plays associated sound byte
 *
 * Notes: N/A
 *
 *********************************************************************/

- (IBAction)labelPressed:(id)sender
{
    UIButton *temp = (UIButton *)sender;
    int letterId = temp.tag;
    NSString *toneFilename;
    if(VCPrintFlag)
        NSLog(@"Label: %d\n",temp.tag);   
    
    switch (letterId) {
        case 0:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label00"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 1:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label01"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 2:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label02"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 3:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label03"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 4:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label04"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 5:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label05"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 6:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label06"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 7:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label07"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 8:
        {
            if(languageFlag == ARABIC)   
                toneFilename = [NSString stringWithFormat:@"label08"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }       
        case 9:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label09"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 10:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label10"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 11:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label11"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 12:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label12"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 13:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label13"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 14:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label14"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 15:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label15"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 16:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label16"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 17:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label17"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 18:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label18"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 19:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label19"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 20:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label20"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 21:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label21"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 22:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label22"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 23:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label23"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 24:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label24"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 25:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label25"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 26:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label26"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        case 27:
        {
            if(languageFlag == ARABIC)
                toneFilename = [NSString stringWithFormat:@"label27"];
            
            NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:toneFilename
                                                        withExtension:@"mp3"];
            SystemSoundID toneSSID = 0;
            AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
            AudioServicesPlaySystemSound(toneSSID);
            
            break;
        }
        default:
            break;
    }
    
}

- (IBAction)infoPressed:(id)sender {
    
    [self showCredits];
}

@end
