//
//  whiteBoard.h
//  Arabic Alphabet Soundboard
//
//  Created by Nick Alto on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#define WBVPrintFlag 0

@interface whiteBoardView : UIView  {
    
    void *cacheBitmap;              // Used to allocate memory for cacheContext
    CGContextRef cacheContext;      // Used as the buffer for drawing onto the UIView
    CGPoint point0;                 
    CGPoint point1;
    CGPoint point2;
    CGPoint point3;
    int eraseMode;                  // Flag to Erase/draw white over current context
    int drawMode;                   // Flag to draw in black 
    int clearFlag;                  // Flag to clear entire context
    NSString *moifile;
    NSString *docs;
    NSFileManager *fm;
    UIImage *img;
    UIAlertView *prompt;
    NSData *data;
    int dataCounter;

}

@property (nonatomic) CGContextRef cacheContext;

// Public Methods
-(id)initWithFrame:(CGRect)frame andContext:(CGContextRef) ctx;
-(void)erase;
-(void)draw;
-(void)clear;
-(void)turnPage:(int)index;
-(void)saveImage;
-(CGContextRef)returnCache;

@end
