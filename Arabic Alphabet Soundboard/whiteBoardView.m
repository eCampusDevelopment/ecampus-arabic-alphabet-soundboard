//
//  whiteBoard.m
//  Arabic Alphabet Soundboard
//
//  Created by Nick Alto on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "whiteBoardView.h"


@implementation whiteBoardView

@synthesize cacheContext    = _cacheContext;

/*********************************************************************
 * Purpose: Initialize flags/context
 *
 * Notes: Needs to call initContext to allocate bitmap buffer
 *
 *********************************************************************/
int flag = 0;
- (id)initWithFrame:(CGRect)frame andContext:(CGContextRef)ctx {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        drawMode    = 1;
        eraseMode   = 0;
        cacheContext = ctx; 
        clearFlag = 0;
        img = [[UIImage alloc]init];
        
        if(cacheContext == nil)
            [self initContext:frame.size];
        prompt = [[UIAlertView alloc]initWithTitle:@"Erase Drawing" message:@"Are you sure you want to erase everything?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        fm = [[NSFileManager alloc]init];
        data = [[NSData alloc]init];
        dataCounter = 0;


    }
    return self;
}

-(CGContextRef)returnCache {
    
    return cacheContext;
}

/*********************************************************************
 * Purpose: Initialize context for backing bitmap
 *
 * Notes: Code from http://blog.effectiveui.com/?p=8105 walkthrough
 * provided by Sean Christmann
 *
 *********************************************************************/

- (void) initContext:(CGSize)size {
    
	int bitmapByteCount;
	int	bitmapBytesPerRow;
	
	// Declare the number of bytes per row. Each pixel in the bitmap in this
	// example is represented by 4 bytes; 8 bits each of red, green, blue, and
	// alpha.
	bitmapBytesPerRow = (size.width * 4);
	bitmapByteCount = (bitmapBytesPerRow * size.height);
	
	// Allocate memory for image data. This is the destination in memory
	// where any drawing to the bitmap context will be rendered.
	cacheBitmap = malloc( bitmapByteCount );
	if (cacheBitmap == NULL) {
        
        if(WBVPrintFlag)
            NSLog(@"ERROR INVALID BITMAP\n");
        return;
	}
	cacheContext = CGBitmapContextCreate (cacheBitmap, size.width, size.height, 8, bitmapBytesPerRow, CGColorSpaceCreateDeviceRGB(), kCGImageAlphaPremultipliedFirst);
	
}

/*********************************************************************
 * Purpose: Set CGPoints when touches began in the UIView
 *
 * Notes: N\A
 *
 *********************************************************************/

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    point0 = [touch locationInView:self];
    point1 = [touch locationInView:self]; // previous previous point
    point2 = [touch locationInView:self]; // previous touch point
    point3 = [touch locationInView:self]; // current touch point
    if(flag == 0)
    {
        CGContextSetFillColorWithColor(cacheContext, [[UIColor whiteColor]CGColor]);
        CGContextFillRect(cacheContext, self.bounds);
        flag = 1;
    }
}

/*********************************************************************
 * Purpose: Set CGPoints when touches Moved in the UIView
 *
 * Notes: N\A
 *
 *********************************************************************/

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    point0 = point1;
    point1 = point2;
    point2 = point3;
    point3 = [touch locationInView:self];
    [self drawToCache];
}

/*********************************************************************
 * Purpose: Drawing algorithm/calculation for smooth drawing
 *
 * Notes: Code from http://blog.effectiveui.com/?p=8105 walkthrough
 * provided by Sean Christmann
 *
 *********************************************************************/

- (void) drawToCache {
    
    if(WBVPrintFlag)
        NSLog(@"drawmode = %d   erasemode = %d  clear = %d\n", drawMode, eraseMode, clearFlag); 
    
    UIColor *color = [UIColor blackColor];
    
    CGContextSetStrokeColorWithColor(cacheContext, [color CGColor]);
    CGContextSetLineCap(cacheContext, kCGLineCapButt);
    CGContextSetLineWidth(cacheContext, 2);
    if(drawMode) {
        
        CGContextSetLineWidth(cacheContext, 2);
        CGContextSetStrokeColorWithColor(cacheContext, [color CGColor]);
        
    } else if(eraseMode) {
        
        CGContextSetStrokeColorWithColor(cacheContext, [[UIColor whiteColor]CGColor]);
        CGContextSetLineWidth(cacheContext, 25);
    }
    
    double x0 = (point0.x > -1) ? point0.x : point1.x; //after 4 touches we should have a back anchor point, if not, use the current anchor point
    double y0 = (point0.y > -1) ? point0.y : point1.y; //after 4 touches we should have a back anchor point, if not, use the current anchor point
    double x1 = point1.x;
    double y1 = point1.y;
    double x2 = point2.x;
    double y2 = point2.y;
    double x3 = point3.x;
    double y3 = point3.y;
    // Assume we need to calculate the control
    // points between (x1,y1) and (x2,y2).
    // Then x0,y0 - the previous vertex,
    //      x3,y3 - the next one.
    
    double xc1 = (x0 + x1) / 2.0;
    double yc1 = (y0 + y1) / 2.0;
    double xc2 = (x1 + x2) / 2.0;
    double yc2 = (y1 + y2) / 2.0;
    double xc3 = (x2 + x3) / 2.0;
    double yc3 = (y2 + y3) / 2.0;
    
    double len1 = sqrt((x1-x0) * (x1-x0) + (y1-y0) * (y1-y0));
    double len2 = sqrt((x2-x1) * (x2-x1) + (y2-y1) * (y2-y1));
    double len3 = sqrt((x3-x2) * (x3-x2) + (y3-y2) * (y3-y2));
    
    double k1 = len1 / (len1 + len2);
    double k2 = len2 / (len2 + len3);
    
    double xm1 = xc1 + (xc2 - xc1) * k1;
    double ym1 = yc1 + (yc2 - yc1) * k1;
    
    double xm2 = xc2 + (xc3 - xc2) * k2;
    double ym2 = yc2 + (yc3 - yc2) * k2;
    double smooth_value = 0.8;
    // Resulting control points. Here smooth_value is mentioned
    // above coefficient K whose value should be in range [0...1].
    float ctrl1_x = xm1 + (xc2 - xm1) * smooth_value + x1 - xm1;
    float ctrl1_y = ym1 + (yc2 - ym1) * smooth_value + y1 - ym1;
    
    float ctrl2_x = xm2 + (xc2 - xm2) * smooth_value + x2 - xm2;
    float ctrl2_y = ym2 + (yc2 - ym2) * smooth_value + y2 - ym2;
    
    CGContextMoveToPoint(cacheContext, point1.x, point1.y);
    CGContextAddCurveToPoint(cacheContext, ctrl1_x, ctrl1_y, ctrl2_x, ctrl2_y, point2.x, point2.y);
    CGContextStrokePath(cacheContext);
    
    CGRect dirtyPoint1 = CGRectMake(point1.x-10, point1.y-10, 20, 20);
    CGRect dirtyPoint2 = CGRectMake(point2.x-10, point2.y-10, 20, 20);
    
    [self setNeedsDisplayInRect:CGRectUnion(dirtyPoint1, dirtyPoint2)];
}

/*********************************************************************
 * Purpose: Draws backing context or cacheContext into the current 
 * Context provided by drawRect, thank you Core Graphics
 *
 * Notes: N\A
 *
 *********************************************************************/

- (void) drawRect:(CGRect)rect  {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGImageRef cacheImage = CGBitmapContextCreateImage(cacheContext);
    if(!clearFlag)
    {
        CGContextDrawImage(context, self.bounds, cacheImage);
    }
    

    if(clearFlag) {
        
        CGContextFlush(context);
        CGContextFlush(cacheContext);
        CGContextClearRect(cacheContext, self.bounds);
        self.backgroundColor = [UIColor whiteColor];
        clearFlag = 0;
    }
    
    CGImageRelease(cacheImage);
}

/*********************************************************************
 * Purpose: Change drawing mode, when button is pressed
 *
 * Notes: N\A
 *
 *********************************************************************/

-(void)erase {
    
    drawMode = 0;
    eraseMode = 1;
}

/*********************************************************************
 * Purpose: Change drawing mode, when button is pressed
 *
 * Notes: N\A
 *
 *********************************************************************/

-(void)draw {
    
    drawMode = 1;
    eraseMode = 0;
}

/*********************************************************************
 * Purpose: Clears drawing context when button is pressed
 *
 * Notes: Currently has to be tapped twice for the screen to be erased
 * might have something to do with the double buffering of drawing.
 *
 *********************************************************************/

-(void)clear {
    
        [prompt show];
       
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {

    
    if (buttonIndex == 0)
    {
        NSLog(@"ok");
        clearFlag = 1;
        [self clearsContextBeforeDrawing];
        [self setNeedsDisplay];                
    }
    else
    {
       
    }
}

-(void)saveImage
{
    CGContextSaveGState(cacheContext);
    CGContextTranslateCTM(cacheContext, 0.0, 1.57);
    CGContextScaleCTM(cacheContext, 1.0, -1.0);
    img =  [UIImage imageWithCGImage:CGBitmapContextCreateImage(cacheContext)];
    CGContextRestoreGState(cacheContext);

    NSLog(@"called\n");
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    docs = [paths  objectAtIndex:0];
    NSError *err;
    NSString *myFolder = [docs stringByAppendingFormat:@"/Data"];
    [fm createDirectoryAtPath:docs withIntermediateDirectories:NO attributes:nil error:&err];
    
    BOOL exists = [fm fileExistsAtPath:myFolder];
    
    if(!exists)
    {
        err = nil;
        NSLog(@"creating folder %@\n", myFolder);
        [fm createDirectoryAtPath:myFolder withIntermediateDirectories:NO attributes:nil error:&err];
    }

    moifile = [myFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"image%d.png", dataCounter]];
    exists = [fm fileExistsAtPath:moifile];
    if(exists)
    {
        [fm removeItemAtPath:moifile error:&err];
    }
    NSLog(@"moifile = %@  myfolder = %@  exists = %d\n", moifile, myFolder, exists);

    if(img != NULL)
    {
        NSLog(@"img NOT NULL\n");
        data = [NSData dataWithData:UIImagePNGRepresentation(img)];
        [data writeToFile:moifile atomically:YES];
    }
    dataCounter++;
}

-(void)turnPage:(int)index {
    
    
}

@end

