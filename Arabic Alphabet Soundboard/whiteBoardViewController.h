//
//  whiteBoardViewControllerViewController.h
//  Arabic Alphabet Soundboard
//
//  Created by Nick Alto on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> 
#import "whiteBoardView.h"

#define WBVCPrintFlag 0

@interface whiteBoardViewController : UIViewController <UIScrollViewDelegate> {
    
    UIButton *returnButton;             // Toggles between Soundboard and Whiteboard
    whiteBoardView *whiteboard;         // Whiteboard UIView
    UIButton *eraseMode;                // Erase button
    UIButton *drawMode;                 // Draw button
    UIButton *clear;                    // Clear button
    UIButton *email;                    // Email button
    CGContextRef cacheContext;          // whiteboard context
    UIButton *indicator;                // Indicator button showing current mode
    UIScrollView *scrollView;
    int whiteBoardType;
}

@property (nonatomic) int whiteBoardType;
@property (nonatomic, strong) whiteBoardView *whiteboard;
// Public Methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andContext:(CGContextRef)ctx andFlag:(int)flag;

@end
