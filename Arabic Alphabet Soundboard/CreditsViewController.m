//
//  CreditsViewController.m
//  Arabic Alphabet Soundboard
//
//  Created by Nick Alto on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CreditsViewController.h"

@implementation CreditsViewController
@synthesize button = _button;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"creditsBackground.png"]];
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    
    UIButton *buttonTitle = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonTitle.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, 40, 79);
    buttonTitle.backgroundColor = [UIColor clearColor];
    buttonTitle.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:65.0];
    buttonTitle.titleLabel.textAlignment = UITextAlignmentLeft;
    [buttonTitle setTitleColor:[UIColor colorWithRed:.745 green:.7019 blue:.917 alpha:1] forState:UIControlStateNormal];
    [buttonTitle setTitle:@"Credits" forState:UIControlStateNormal];
    [buttonTitle setUserInteractionEnabled:NO];
    [self.view addSubview:buttonTitle];
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload {
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    if(interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
        return TRUE;
    else 
        return FALSE;
}

-(IBAction)dismissCredits:(id)sender {
    
    [self dismissModalViewControllerAnimated:YES];
}
@end
